<?php

namespace App\Http\Controllers\tineye;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use League\Flysystem\Filesystem;
use Image;

class ProcessController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        $this->s3 = Storage::disk('s3');
    }


    public function index(){

    }

    public function upload(Request $request){

        $urlString = '';
        $catalog_image_path = '';

        //If Catalog url avialable
        if ($request->has('catlogUrl')) {

            $catalogueImgUrl = $request->input('catlogUrl');

            $image = $catalogueImgUrl;
            $image_orginal = $catalogueImgUrl;
            $catalog_image_path = $image_orginal;

            # Resize background as per Tineye scaling (3 time up to 2000px for shortest dimension)

            list($original_width, $original_height) = getimagesize($image);

            $shortest_dimension = min($original_width, $original_height);
            $scaled_shortest_dimension = max($shortest_dimension,2000);

            if ($shortest_dimension == $original_width) {
                $scaled_width = $scaled_shortest_dimension;
                $scaled_height = intval($original_height * ($scaled_width / $original_width));

            }else{
                $scaled_height = $scaled_shortest_dimension;
                $scaled_width = intval($original_width * ( $scaled_height / $original_width));
            }

            $imageLocalPath = basename($image);

            Storage::disk('local')->put($imageLocalPath, file_get_contents($image));

            // resize image to fixed size
            $img = Image::make(storage_path('/public/').$imageLocalPath);
            $img->resize($scaled_width, $scaled_height)->save();

            //Image one

            $imageName = env('AWS_SUB_BUCKET').'/'. basename($image);

            Storage::disk('s3')->put($imageName, file_get_contents(storage_path('/public/').$imageLocalPath));
            Storage::disk('s3')->setVisibility($imageName, 'public');

            //Image two

            $imageNameOrginal = env('AWS_SUB_BUCKET').'/orginals/' .  basename($image);

            Storage::disk('s3')->put($imageNameOrginal, file_get_contents(storage_path('/public/').$imageLocalPath));
            Storage::disk('s3')->setVisibility($imageNameOrginal, 'public');

            $urlString = 'url='.env('INSIGHT_GET_IMAGE_CLOUD').'/'.env('AWS_BUCKET').'/'.env('AWS_SUB_BUCKET').'/'.basename($image);

        }else{

            $this->validate($request, [
                'catlogueImage'=> 'required | mimes:jpeg,jpg,png',
            ]);

            if ($request->hasFile('catlogueImage')) {

                $image = $request->catlogueImage;
                $image_orginal = $request->catlogueImage;

                # Resize background as per Tineye scaling (3 time up to 2000px for shortest dimension)

                list($original_width, $original_height) = getimagesize($image);

                $shortest_dimension = min($original_width, $original_height);
                $scaled_shortest_dimension = max($shortest_dimension,2000);

                if ($shortest_dimension == $original_width) {
                    $scaled_width = $scaled_shortest_dimension;
                    $scaled_height = intval($original_height * ($scaled_width / $original_width));

                }else{
                    $scaled_height = $scaled_shortest_dimension;
                    $scaled_width = intval($original_width * ( $scaled_height / $original_width));
                }

                //Image one

                $img = Image::make($image->getRealPath());

                // resize image to fixed size
                $img->resize($scaled_width, $scaled_height)->save();


                $imageName = env('AWS_SUB_BUCKET').'/' . $image->getClientOriginalName();

                Storage::disk('s3')->put($imageName, file_get_contents($image));
                Storage::disk('s3')->setVisibility($imageName, 'public');

                //Image two

                $img_orginal = Image::make($image_orginal->getRealPath());

                // resize image to fixed size
                $img_orginal->resize($scaled_width, $scaled_height)->save();

                $imageNameOrginal = env('AWS_SUB_BUCKET').'/orginals/' . $image_orginal->getClientOriginalName();

                $catalog_image_path = env('INSIGHT_GET_IMAGE_CLOUD').'/'.env('AWS_BUCKET').'/'.env('AWS_SUB_BUCKET').'/'.$image->getClientOriginalName();

                Storage::disk('s3')->put($imageNameOrginal, file_get_contents($image_orginal));
                Storage::disk('s3')->setVisibility($imageNameOrginal, 'public');


                $urlString = 'url='.env('INSIGHT_GET_IMAGE_CLOUD').'/'.env('AWS_BUCKET').'/'.env('AWS_SUB_BUCKET').'/'.$image->getClientOriginalName();
                //$urlString = 'url=https://s3-ap-southeast-2.amazonaws.com/insights-retail-catalogue-upload/AU_Dan_Murphys_20170824_20170903_Unbeatable_For_Fathers_Day_page016.jpg';


            }
        }


        $catalogueminScroe = $request->input('catMinScore');
        $urlString .= '&min_score='.$catalogueminScroe;

        $catalogueResult = $request->input('catResult');
        $urlString .= '&limit='.$catalogueResult;

        $urlString .= '&generate_overlay =true';

        $this->serchImagePathRequest($urlString, $catalog_image_path);
        //$this->addImageRequest($urlString);
        //dd($request->all());

    }

    public function serchImagePathRequest($postData, $catlogImgPath = ''){

        $service_url = 'https://matchengine.tineye.com/invigor_group/rest/search/';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "invigor_group:eJLJ2FCjxya2"); //Your credentials goes here
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
//        curl_setopt(
//            $curl,
//            CURLOPT_POSTFIELDS,'url=https://s3-ap-southeast-2.amazonaws.com/insights-retail-alcohol/images/10001_small.png'
//        );
        curl_setopt(
            $curl,
            CURLOPT_POSTFIELDS,$postData
        );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //IMP if the url has https and you don't want to verify source certificate

        $curl_response = curl_exec($curl);
        //$info = curl_getinfo($curl);
        $response = json_decode($curl_response, true);
        //echo('<pre>'); print_r($response); die();

        if(!empty($response['result'])){
            foreach($response['result'] as $key => $element){
                $str = explode('_',$element['filepath']);
                $product_code = $str['0'];

                if(is_numeric($product_code)){

                    //Finding a product Code
                    $product_info = $this->csvToArray(storage_path('public/alcohol_data/'.env('ALCOHOL_CSV')));

                    if($product_info){
                        foreach( $product_info as $k => $val ){

                            if(!empty($val['Product Code']) AND ($val['Product Code'] == $product_code)){
                                $response['result'][$key]['id'] = $product_code;
                                $response['result'][$key]['name'] = $val['Name'];
                            }
                        }
                    }

                }
            }
        }

        if(!empty($catlogImgPath)){
            $response['catlogImg'][0]['path'] = $catlogImgPath;
        }

        //Product Code
        //$product_info = $this->csvToArray(storage_path('public/alchol_data/'.'20170606 ALCOHOL BaseProduct Extract.csv'));

        $curl_response = json_encode($response);
        print_r($curl_response); die();
        echo $curl_response;
       // return response()->json($curl_response);

    }

    public function addImageRequest($postData){
        $service_url = 'https://matchengine.tineye.com/invigor_group/rest/add/';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "invigor_group:eJLJ2FCjxya2"); //Your credentials goes here
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt(
            $curl,
            CURLOPT_POSTFIELDS,'url=https://s3-ap-southeast-2.amazonaws.com/insights-retail-alcohol/images/10001_small.png&filepath=test'
            );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //IMP if the url has https and you don't want to verify source certificate

        $curl_response = curl_exec($curl);
        $response = json_decode($curl_response);
        $info = curl_getinfo($curl);
        curl_close($curl);

        return response()->json($response);
    }

    public function drawRectangle(Request $request){
        ini_set('max_execution_time', 0);
        $x1 = $request->input('x1');
        $x2 = $request->input('x2');
        $y1 = $request->input('y1');
        $y2 = $request->input('y2');
        $prod_image = $request->input('id');
        //dd($x1,$x2,$y1, $y2);
        // get the image from s3
        $cat_image_path = $request->input('cat_image_path');
        $imageName = basename($cat_image_path);

        // get the content of that image
        $content = file_get_contents($cat_image_path);

        // get the new file and make image out of it
        $img = Image::make($content);

        //        return response()->json([
        //            'data' => print_r($img)
        //        ]);

        $error_factor = 0;

        $img->rectangle($x1+($error_factor), $y1+($error_factor), $x2+($error_factor), $y2+($error_factor), function ($draw) {
            $draw->background('rgba(255, 255, 255, 0.5)');
            $draw->border(2, '#ccc');
        })->stream();
        //dd($x1+($error_factor),$y1+($error_factor),$x2+($error_factor),$y2+($error_factor));
        $p_width = abs(($x2+($error_factor)) - $x1+($error_factor));
        $p_height = abs(($y2+($error_factor)) - $y1+($error_factor));

        //$img->fill('https://www.invigorgroup.com/wp-content/uploads/2017/05/Color-Transparent-355w.png', $x1+($error_factor), $y1+($error_factor))->stream();

        \Storage::disk('local')->put($imageName, $img->__toString());


        //        return response()->json([
        //            $img->__toString()
        //        ]);

        //        return response()->json([
        //            'data' => 'done'
        //        ]);


        $product_image_path = "https://s3-ap-southeast-2.amazonaws.com/insights-retail-alcohol/images/".$prod_image;
        // get the content of that image
        $product_img_content = file_get_contents($product_image_path);

        // get the new file and make image out of it
        $product_img = Image::make($product_img_content);
        \Storage::disk('local')->put($prod_image, $product_img->stream()->__toString());
        $product_image_path = storage_path('/public/').$prod_image;

        $imagePathS3 = env('AWS_SUB_BUCKET').'/' . $imageName;
        \Storage::disk('s3')->put($imagePathS3, file_get_contents(storage_path('/public/').$imageName));
        \Storage::disk('s3')->setVisibility($imageName, 'public');

        list($width, $height) = getimagesize($product_image_path);

        $product_width = $p_width;
        $product_height = $p_height;

        //$product_width = intval(($width)*(0.28));
        //$product_height = intval(($height)*(0.28));
        $this->resize( abs($product_width), abs($product_height), storage_path('/public/').$prod_image, storage_path('/public/').$prod_image);
        $this->drawPic(storage_path('/public/').$imageName,storage_path('/public/').$prod_image , $x1+($error_factor),$y1+($error_factor),$x2+($error_factor),$y2+($error_factor));



        list($width, $height) = getimagesize($product_image_path);

        //Previous way the image been overlapped
        //$image = $this->imageFill(storage_path('/public/').$imageName, $product_image_path, $x1, $y1);

        \Storage::disk('s3')->put($imagePathS3, file_get_contents(storage_path('/public/').$imageName));
        \Storage::disk('s3')->setVisibility($imageName, 'public');

        return response()->json([
            'status' => 'completed',
            's3_image_name' => $imageName
        ]);

    }


    public function resize($width = NULL, $height = NULL, $targetFile, $originalFile) {

        $im = imagecreatefrompng($originalFile);
        $cropped = imagecropauto($im, IMG_CROP_DEFAULT);

        if ($cropped !== false) { // in case a new image resource was returned
            imagedestroy($im);    // we destroy the original image
            $im = $cropped;       // and assign the cropped image to $im
        }

        //Remove black background and make it transparent
        $background = imagecolorallocate($im , 0, 0, 0);
        imagecolortransparent($im, $background);
        //

        imagepng($im, $originalFile);
        imagedestroy($im);

        // open an image file
        //$img = Image::make($originalFile)->trim();
        $img = Image::make($originalFile);
        // now you are able to resize the instance
        $img->resize($width, $height);
        // finally we save the image as a new file

        // crop image
        //$img->crop($width, $height);

        $img->save($originalFile);
    }


    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }


    public function imageFill($dst_im, $src_im, $dst_x, $dst_y){

        //set the source image (foreground)
        $sourceImage = $src_im;

        //set the destination image (background)
        $destImage = $dst_im;

        //get the size of the source image, needed for imagecopy()
        list($srcWidth, $srcHeight) = getimagesize($sourceImage);

        //create a new image from the source image
        $src = imagecreatefrompng($sourceImage);

        //create a new image from the destination image
        $dest = imagecreatefromjpeg($destImage);

        //set the x and y positions of the source image on top of the destination image
        $src_xPosition = $dst_x; //75 pixels from the left
        $src_yPosition = $dst_y; //50 pixels from the top

        //set the x and y positions of the source image to be copied to the destination image
        $src_cropXposition = 0; //do not crop at the side
        $src_cropYposition = 0; //do not crop on the top

        //merge the source and destination images
        imagecopy($dest,$src,$src_xPosition,$src_yPosition,$src_cropXposition,$src_cropYposition,$srcWidth,$srcHeight);

        //output the merged images to a file
        /*
         * '100' is an optional parameter,
         * it represents the quality of the image to be created,
         * if not set, the default is about '75'
         */
        imagejpeg($dest,$dst_im,100);

        //destroy the source image
        imagedestroy($src);

        //destroy the destination image
        imagedestroy($dest);
    }


    public function drawPic($im, $product_img,  $x1, $y1, $x2, $y2){

        $file1 = $im;
        $file2 = $product_img;

        Image::make($product_img);

        // First image

        $image = imagecreatefromjpeg($im);

        // Second image (the overlay)
        $overlay = imagecreatefrompng($file2);

        //For testing the crop background
        //$red = imagecolorallocate($overlay, 255, 0, 0);
        //imagefill($overlay, 0, 0, $red);

        // We need to know the width and height of the overlay
        list($width, $height, $type, $attr) = getimagesize($file2);

        // Apply the overlay
        imagecopy($image, $overlay, $x1, $y1, 0, 0, $width, $height);
        imagedestroy($overlay);

        // Output the results
        header('Content-type: image/jpeg');
        imagejpeg($image,$im,100);
        imagedestroy($image);
    }

    public function SearchProductByArea(Request $request){
        ini_set('max_execution_time', 0);

        $x = $request->input('x');
        $y = $request->input('y');
        $product_width = $request->input('pWidth');
        $product_height = $request->input('pHeight');
        $catlog_image_path = $request->input('catImagePath');
        $catalog_orginal_name = basename($catlog_image_path);
        $selected_prod_image_path = time().'_'.$catalog_orginal_name;

        $files = glob(storage_path('/public/area_search_img/').'*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }

        // get the content of that image
        $catalog_img_content = file_get_contents($catlog_image_path);
        // get the catalog and make image out of it
        $catalog_img = Image::make($catalog_img_content);

        \Storage::disk('local')->put('/area_search_img/'.$selected_prod_image_path, $catalog_img->stream()->__toString());
        $sel_product_image_path = storage_path('/public/area_search_img/').$selected_prod_image_path;

        // open file a image resource
        $img = Image::make($sel_product_image_path);

        // crop image
        $img->crop($product_width, $product_height, $x, $y);
        $img->save();

        if(file_exists($sel_product_image_path)){

            $AWS_product_img_path = env('AWS_SUB_BUCKET').'/select_area_product_images/' .  basename($sel_product_image_path);

            Storage::disk('s3')->put($AWS_product_img_path, file_get_contents($sel_product_image_path));
            Storage::disk('s3')->setVisibility($AWS_product_img_path, 'public');

            $search_string = 'url='.env('INSIGHT_GET_IMAGE_CLOUD').'/'.env('AWS_BUCKET').'/'.$AWS_product_img_path.'&min_score=0&limit=-1';

            $response = $this->serchImagePathRequest($search_string, $catlog_image_path);
        }
    }

}
