/* File: gulpfile.js */

var del = require('del');
var elixir = require('laravel-elixir');
var gulp = require('gulp');
var task = elixir.Task;

elixir.config.css.minifyCss.pluginOptions.processImport = false;
elixir.config.js.browserify.watchify.options.poll = true;

elixir.config.sourcemaps = false;

elixir.extend('remove', function(path) {
    new task('remove', function() {
        return del(path);
    });
});



/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    // cleanup
    mix.remove([
        'public/css',
        'public/images',
        'public/js',
        'resources/assets/css/compiled'
    ]);

    mix.less('resources/assets/less/main.less', 'resources/assets/css/compiled/main.css');

    // combine css for the whole application
    mix.styles([
        'resources/assets/css/vendor/bootstrap/bootstrap.min.css',
        'resources/assets/css/compiled/main.css',
    ], 'public/css/main.css', './');

    // combine scripts for the whole application except reports, heatmaps, flowmaps
    mix.scripts([
        'resources/assets/js/jquery-2.2.3.min.js',
        'resources/assets/js/vendor/tether/tether.min.js',
        'resources/assets/js/vendor/bootstrap/bootstrap.min.js',
        'resources/assets/js/main.js'
    ], 'public/js/main.js', './');

    mix.scripts('resources/assets/js/vendor/crop-select/crop-select-js.min.js', 'public/js/crop-select-js.min.js');
    mix.styles('resources/assets/css/vendor/crop-select/crop-select-js.min.css', 'public/css/crop-select-js.min.css');
    mix.copy('resources/assets/css/vendor/crop-select/img', 'public/css/img/');

});