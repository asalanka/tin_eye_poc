<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/search', function () {
    return view('index');
});


Route::post('/process/upload', [
    'uses' => 'tineye\ProcessController@upload',
    'as' => 'process.upload'
]);

Route::post('/draw/{id}', [
    'uses' => 'tineye\ProcessController@drawRectangle',
    'as' => 'process.draw'
]);

Route::post('/area-search', [
    'uses' => 'tineye\ProcessController@SearchProductByArea',
    'as' => 'process.areaSearch'
]);