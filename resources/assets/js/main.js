/**
 * Created by asanka.rathnayake on 21/11/2017.
 */
$(document).on('click', '#close-preview', function(){
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
            $('.image-preview').popover('show');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});

function bs_input_file() {
    $(".input-file").before(
        function() {
            if ( ! $(this).prev().hasClass('input-ghost') ) {
                var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                element.attr("name",$(this).attr("name"));
                element.change(function(){
                    element.next(element).find('input').val((element.val()).split('\\').pop());
                });
                $(this).find("button.btn-choose").click(function(){
                    element.click();
                });
                $(this).find("button.btn-reset").click(function(){
                    element.val(null);
                    $(this).parents(".input-file").find('input').val('');
                });
                $(this).find('input').css("cursor","pointer");
                $(this).find('input').mousedown(function() {
                    $(this).parents('.input-file').prev().click();
                    return false;
                });
                return element;
            }
        }
    );
}
$(function() {
    bs_input_file();
});

$(function() {
    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Browse");
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function (){
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text("Change");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }
        reader.readAsDataURL(file);
    });

    $('#searchBtn').click(function(){
        $('.canvas').hide();
        $('.input-area').show();
    });

//Application functionlity
    var Global = '';

    $('#upload-form').submit(function(e) {
        e.preventDefault();

        var formData = {
            'cat_result': $('input[name=cat-result]').val(),
        };

        $('#upload-form .alert').hide();

        $.ajax({
            url: "/process/upload",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){
                $('.loading-overlay').show();
            },
            contentType: false,
            processData:false,
            success: function(data)
            {
                try {
                    JSON.parse(data);
                } catch (e) {
                    $('#upload-form .alert-info').show();
                    $('.loading-overlay').hide();
                    return false;
                }

                $('.loading-overlay').hide();
                var imgUrl = $("input[name=catlogUrl]").val();
                var img = $('<img />', {src : imgUrl });
                $('.catlogue-screen').html('');
                $('.catlogue-screen').append(img);
                $('.input-area').hide();
                $('.canvas').show();
                Global = data;
                $.each(JSON.parse(data), function(index, element) {

                    if(index =='query_image'){
                        $.each(element, function(key, el) {
                            img = $('<img />', {src : s3.s3Path+'/'+el });
                            $('.catlogue-screen').html('');
                            $('.catlogue-screen').append(img);
                            $('.catlogue-screen').attr('image_path', s3.s3Path+'/orginals/'+el);
                        });
                    }
                    if(index =='result'){

                        $('.scrollable-menu #table-hold').html('');

                        if(element.length < 1){
                            $('.scrollable-menu #table-hold').append('<tr><td>No results found!</td></tr>');
                        }

                        $.each(element, function(key, el) {
                            $('.scrollable-menu #table-hold').append('<tr><td>'+el.id+'</td><td><div class="prod_name">'+el.name+'</div></td><td>'+el.score+'</td><td><div id='+el.filepath+' class="hidden_preview"  x1="'+el.query_overlap_rect.left+'" x2="'+el.query_overlap_rect.right+'"   y1="'+el.query_overlap_rect.top+'" y2="'+el.query_overlap_rect.bottom+'"><img id="'+el.filepath+'_img" src="https://s3-ap-southeast-2.amazonaws.com/insights-retail-alcohol/images/'+el.filepath +'"    style="max-height: 100px; max-width: 100px;"   /></div></td></tr>');
                        });
                    }
                });

            },
            error: function()
            {
            }
        });
    });


    $(".scrollable-menu").on('click', '.hidden_preview', function () {
        $('#croppedSearchBtn').prop("disabled", true);
        var csrf_token = $('meta[name="_token"]').attr('content');

        var id =  $(this).attr('id');
        var x1 = $(this).attr('x1');
        var x2 = $(this).attr('x2');
        var y1 = $(this).attr('y1');
        var y2 = $(this).attr('y2');
        var cat_image = $('.catlogue-screen').attr('image_path');

        $.ajax({
            url: "/draw/"+id,
            type: "POST",
            dataType:'json',
            data: {
                id: id,
                x1:x1,
                x2:x2,
                y1:y1,
                y2:y2,
                cat_image_path:cat_image,

            },

            headers: { 'X-CSRF-TOKEN': csrf_token },
            beforeSend: function(){
                $('.loading-overlay').show();
            },
            success: function(data)
            {   //console.log(Global);
                $('.loading-overlay').hide();
                $('.catlogue-screen').html('');

                img = $('<img />', {src : s3.s3Path+'/'+data.s3_image_name+ '?' + (new Date()).getTime() });

                img.appendTo($('.catlogue-screen'));
                $('.catlogue-screen').attr('image_path', s3.s3Path+'/orginals/'+data.s3_image_name);

                $('#resetBtn').on('click', function (e) {

                    $.each(JSON.parse(Global), function(index, element) {

                        if(index =='query_image'){
                            $.each(element, function(key, el) {
                                img = $('<img />', {src : s3.s3Path+'/'+el });
                                $('.catlogue-screen').html('');
                                $('.catlogue-screen').append(img);
                                $('.catlogue-screen').attr('image_path', s3.s3Path+'/orginals/'+el);
                            });
                        }
                        if(index =='result'){

                            $('.scrollable-menu #table-hold').html('');

                            if(element.length < 1){
                                $('.scrollable-menu #table-hold').append('<tr><td>No results found!</td></tr>');
                            }

                            $.each(element, function(key, el) {
                                $('.scrollable-menu #table-hold').append('<tr><td>'+el.id+'</td><td><div class="prod_name">'+el.name+'</div></td><td>'+el.score+'</td><td><div id='+el.filepath+' class="hidden_preview"  x1="'+el.query_overlap_rect.left+'" x2="'+el.query_overlap_rect.right+'"   y1="'+el.query_overlap_rect.top+'" y2="'+el.query_overlap_rect.bottom+'"><img id="'+el.filepath+'_img" src="https://s3-ap-southeast-2.amazonaws.com/insights-retail-alcohol/images/'+el.filepath +'"    style="max-height: 100px; max-width: 100px;"   /></div></td></tr>');
                            });
                        }
                    });

                });

            },
            error: function()
            {
            }
        });
    });


    $(document).on('click', '.catlogue-screen img', function(e) {
        var offset = $(this).offset();
        var X = (e.pageX - offset.left);
        var Y = (e.pageY - offset.top);

        var viewHeight = $('.catlogue-screen img').height();
        var viewWidth = $('.catlogue-screen img').width();

        var image = new Image();
        image.src = $('.catlogue-screen img').attr("src");

        image.onload = function() {
            var naturalHeight = this.height;
            var naturalWidth = this.width;


            //Calculating the acctual ratio
            var heightRatio = (naturalHeight/viewHeight).toFixed(2);
            var widthRatio = (naturalWidth/viewWidth).toFixed(2);
            //console.log('Ratio H :'+ heightRatio+' Ratio W:'+widthRatio);
            var actualX = (X*widthRatio);
            var actualY = (Y*heightRatio);

            $.each(JSON.parse(Global), function(index, element) {

                if(index =='result'){

                    $('.scrollable-menu #table-hold').html('');

                    var $i = 0;
                    $.each(element, function(key, el) {

                        if( (actualX >= el.query_overlap_rect.left && actualX <= el.query_overlap_rect.right) && ( actualY >= el.query_overlap_rect.top && actualY <= el.query_overlap_rect.bottom) )
                        {
                            $i++;
                            $('.scrollable-menu #table-hold').append('<tr><td>' + el.id + '</td><td><div class="prod_name">' + el.name + '</div></td><td>' + el.score + '</td><td><div id=' + el.filepath + ' class="hidden_preview"  x1="' + el.query_overlap_rect.left + '" x2="' + el.query_overlap_rect.right + '"   y1="' + el.query_overlap_rect.top + '" y2="' + el.query_overlap_rect.bottom + '"><img id="' + el.filepath + '_img" src="https://s3-ap-southeast-2.amazonaws.com/insights-retail-alcohol/images/' + el.filepath + '"    style="max-height: 100px; max-width: 100px;"   /></div></td></tr>');
                        }

                    });

                    if($i < 1){
                        $('.scrollable-menu #table-hold').append('<tr><td>No results found!</td></tr>');
                    }
                }
            });
        };

    });

});