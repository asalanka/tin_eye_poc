<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="_token" content="{{ csrf_token() }}">
    <meta Http-Equiv="Cache" content="no-cache">
    <meta Http-Equiv="Pragma-Control" content="no-cache">
    <meta Http-Equiv="Cache-directive" Content="no-cache">
    <meta Http-Equiv="Pragma-directive" Content="no-cache">
    <meta Http-Equiv="Cache-Control" Content="no-cache">
    <meta Http-Equiv="Pragma" Content="no-cache">
    <meta Http-Equiv="Expires" Content="0">
    <meta Http-Equiv="Pragma-directive: no-cache">
    <meta Http-Equiv="Cache-directive: no-cache">
    <title>Tin Eye - @yield('title')</title>

    {{--@yield('header-scripts')--}}
    <link rel="stylesheet" href="{{ elixir("css/main.css") }}">
    {{--@yield('styles')--}}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <script type="text/javascript">
        var s3 = {
            s3Path : '{{ env('INSIGHT_GET_IMAGE_CLOUD').'/'.env('AWS_BUCKET').'/'.env('AWS_SUB_BUCKET') }}'
        }

    </script>

    <!--[if lt IE 9]>
    <script src="{{ asset('js/main.js') }}"></script>

    <![endif]-->

    <meta name="_token" content="{{ csrf_token() }}">

    @yield('header-scripts')
</head>

<body class="container">

<div id="wrapper" class="wrapper">

    <div class="loading-overlay">
        <div class="spin-loader"></div>
    </div>
    <!-- Header -->
{{--@include('partials.default-header')--}}

<!-- Left Sidebar -->
{{--//@include('partials.default-left-sidebar')--}}

<!-- Content Wrapper. Contains page content -->

    @yield('content')


    <!-- Footer -->
    {{--@include('partials.default-footer')--}}
</div><!-- ./wrapper -->
<script src="{{ elixir("js/main.js") }}"></script>
{{--@yield('scripts')--}}
</body>

</html>