<div class="canvas" style="display: none;">
    <div class="row">
        <div class="col-md-12 search-btn">
            <button type="button" id="searchBtn" class="btn btn-warning navbar-btn">Back</button>
            <button type="button" id="resetBtn" class="btn btn-info navbar-btn">Reset</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 catlogue-area">
            <div class="row">
                <div class="catlogue-screen col-md-8">
                    <img src="https://dummyimage.com/1200x800/000/fff" />
                </div>

                <div class="scrollable-menu col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="table-tr">ID</div><div class="table-tr">Name</div><div class="table-tr">Score</div></td>
                                <table class="table table-striped">
                                    <tbody id="table-hold">
                                        <tr><td>..</td><td>..</td><td>..</td><td>..</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>