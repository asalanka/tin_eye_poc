
<div class="input-area">

    {!! Form::open(['url' => '/', 'method' => 'post', 'id' => 'upload-form', 'enctype' => "multipart/form-data"]) !!}
    <fieldset>
        <legend align="right">TinEYE SYSTEM</legend>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 alert alert-info">
                    <strong>Info!</strong> No results being found.
                </div>
                <div class="col-md-12 form-group">
                    <label>Cataloge URL</label>
                    <input type="text" name="catlogUrl" placeholder="Http://" class="form-control">
                </div>
            </div>

            <div class="row">
                <!-- COMPONENT START -->
                <div class="col-xs-12 col-md-12 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="input-group input-file" name="catlogueImage">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-choose" type="button">Choose</button>
                    </span>
                        <input type="text" class="form-control" placeholder='Choose a file...' name="catlogueImage" />
                        <span class="input-group-btn">
                         <button class="btn btn-warning btn-reset" type="button">Reset</button>
                    </span>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6 form-group">
                    <label>Minimum Score (0-100)</label>
                    <input type="text" name="catMinScore" placeholder="0" value="0" class="form-control">
                </div>
                <div class="col-md-6 form-group">
                    <label>Number of Results (-1 for all)</label>
                    <input type="text" name="catResult" placeholder="-1" value="-1" class="form-control">
                </div>

            </div>

            <input type="submit" class="btn btn-info" value="Submit">


        </div>
    </fieldset>
    {!! Form::close() !!}
</div>