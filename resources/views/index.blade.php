@extends('layouts.default')

@section('title', 'TinEye Test System')

@section('content')
    @include('partials.upload-form')
    @include('partials.result-canvas')
@endsection